use std::{
    io::{self, Read},
    net::TcpListener,
};

pub fn main() -> io::Result<()> {
    static_server()?;
    dynamic_server()?;
    Ok(())
}

fn dynamic_server() -> io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:12330")?;

    for stream in listener.incoming() {}

    Ok(())
}

fn static_server() -> io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:12330")?;

    for stream in listener.incoming() {
        let mut bytes: [u8; 5] = [0; 5];
        let mut stream = stream.unwrap();

        stream.read_exact(&mut bytes)?;

        let received = std::str::from_utf8(&bytes).expect("valid utf8");
        eprintln!("{}", received);
    }

    Ok(())
}
